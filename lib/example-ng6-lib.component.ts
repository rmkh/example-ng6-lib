import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'enl-example-ng6-lib',
  template: `
    <p>This is component from NPM Modules</p>
  `,
  styles: []
})
export class ExampleNg6LibComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
